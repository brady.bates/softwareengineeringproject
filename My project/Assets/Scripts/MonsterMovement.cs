using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterMovement : MonoBehaviour
{
    [SerializeField] float moveSpeed = 3f;
    Rigidbody2D rb; // Forces 2d movement
    Transform target; // Decides where to move to
    Vector2 moveDirection; // Helps with movement/direction



    private void Awake() // this loads up the movement on start
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void Start() //this finds the player on start
    {
        target = GameObject.Find("Player").transform; 
    }


    private void Update() //handles calcualtions for rotation and direction
    {
        if (target)
        {
            Vector3 direction = (target.position - transform.position).normalized;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg; // finds angle
            rb.rotation = angle;
            moveDirection = direction;  
        }
    }


    private void FixedUpdate() // this handles movement towards the player
    {
        if (target)
        {
            rb.velocity = new Vector2(moveDirection.x, moveDirection.y) * moveSpeed;

        }
    }


}
