using UnityEngine;

public class DestroyCollision : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D col)

    {
        Destroy(col.gameObject);
    }
}
