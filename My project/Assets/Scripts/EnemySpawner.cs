using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *  This script is used to randomly spawn a "Square" entity around the playfield
 *  At it's current form, it only spawns randomly, does nothing else
 * 
*/
public class EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private GameObject squarePrefab; // All instances of "Square" are placeholder name for future "monster" name
    [SerializeField]
    private float squareInterval = 5; // Time interval

    void Start()
    {
        StartCoroutine(spawnEnemy(squareInterval, squarePrefab)); // Starts the spawns on game start

    }

    private IEnumerator spawnEnemy(float interval, GameObject enemy)
    {
        yield return new WaitForSeconds(interval); // Random x,y spawner
        GameObject newEnemy = Instantiate(enemy, new Vector3(Random.Range(-5f, 5), Random.Range(-6f, 6f), 0), Quaternion.identity);
        StartCoroutine(spawnEnemy(interval, enemy)); //endless spawner might change to have a cap if needed but have to figure that out
    }

}